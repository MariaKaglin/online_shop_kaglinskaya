from django.contrib import admin
from .models import *

admin.site.register(Country)
admin.site.register(Active)
admin.site.register(Thing)
admin.site.register(Trip)
admin.site.register(UserProfile)

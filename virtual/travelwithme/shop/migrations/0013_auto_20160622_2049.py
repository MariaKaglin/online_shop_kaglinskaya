# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0012_auto_20160622_2048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='good_type',
            name='name',
            field=models.CharField(max_length=30),
        ),
    ]

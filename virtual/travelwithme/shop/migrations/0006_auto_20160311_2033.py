# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_userprofile_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='thing',
            name='additional_information',
            field=models.CharField(max_length=400, default=''),
        ),
        migrations.AlterField(
            model_name='thing',
            name='information',
            field=models.CharField(max_length=150),
        ),
    ]

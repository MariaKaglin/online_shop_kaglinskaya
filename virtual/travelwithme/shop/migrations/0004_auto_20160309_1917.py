# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('shop', '0003_thing_photo'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('photo', models.ImageField(upload_to='users')),
                ('information', models.CharField(max_length=200)),
                ('country', models.CharField(max_length=30)),
                ('town', models.CharField(max_length=20)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='thing',
            name='photo',
            field=models.ImageField(default='shop/static/img/Camera-icon.png', upload_to='items'),
        ),
        migrations.AlterField(
            model_name='thing',
            name='seller',
            field=models.ForeignKey(to='shop.UserProfile'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0011_auto_20160622_2044'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recall',
            name='name',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='thing',
            name='name',
            field=models.CharField(max_length=30),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('shop', '0006_auto_20160311_2033'),
    ]

    operations = [
        migrations.AddField(
            model_name='recall',
            name='name',
            field=models.CharField(max_length=30, default=''),
        ),
        migrations.AddField(
            model_name='thing',
            name='tags',
            field=taggit.managers.TaggableManager(verbose_name='Tags', through='taggit.TaggedItem', to='taggit.Tag', help_text='A comma-separated list of tags.'),
        ),
        migrations.AlterField(
            model_name='recall',
            name='to',
            field=models.ForeignKey(to='shop.Thing'),
        ),
        migrations.AlterField(
            model_name='thing',
            name='name',
            field=models.CharField(max_length=30, default=''),
        ),
        migrations.AlterField(
            model_name='thing',
            name='photo',
            field=models.ImageField(upload_to='items', default='shop/static/img/a.png'),
        ),
    ]

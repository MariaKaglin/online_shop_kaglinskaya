# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0007_auto_20160312_0958'),
    ]

    operations = [
        migrations.CreateModel(
            name='Search',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('min_cost', models.IntegerField(default=0)),
                ('max_cost', models.IntegerField(default=0)),
                ('activity_choose', models.ManyToManyField(to='shop.Active')),
                ('country_choose', models.ManyToManyField(to='shop.Country')),
            ],
        ),
        migrations.AddField(
            model_name='item',
            name='type',
            field=models.ForeignKey(default='type', to='shop.Good_type'),
        ),
        migrations.AlterField(
            model_name='good_type',
            name='name',
            field=models.CharField(max_length=30, default=''),
        ),
    ]

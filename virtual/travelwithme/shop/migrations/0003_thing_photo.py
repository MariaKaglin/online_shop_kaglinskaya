# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_good_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='thing',
            name='photo',
            field=models.ImageField(upload_to='items', default='Camera-icon.png'),
        ),
    ]

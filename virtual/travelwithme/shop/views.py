import json
import re
from django.shortcuts import render
from shop.models import *
from shop.models import Active
from shop.models import UserForm
from shop.models import UserProfileForm
from django.template import loader
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from itertools import chain

from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login,
    logout , update_session_auth_hash,
)

def navigation_menu(request):
    search_form = FormSearch()
    return {'countr': Country.objects.all(), 'act': Active.objects.all(), 'search_form':search_form}


def main(request):
    return render(request, 'shop/main_page.html')


def catalog(request):
	items = {}
	return render(request, 'shop/catalog_new.html', { 'items':items})


def basket(request):
	return render(request, 'shop/basket_new.html')


def add(request):
	#добавляем товар в local-storage
    return HttpResponse("Добавлен в корзину")
#def basket_list(request):
#def basket_information(request):
#def basket_pay(request):
#def basket_success(request):

def get_data(request):
    raw_data = request.GET['raw_data']
    items = json.loads(raw_data)
    data = []
    for i in items:
    	item = Thing.objects.get(id=i)
    	data.append([item.name, item.cost, item.information])
    return  HttpResponse(
    	json.dumps(data),
    	content_type="application/json"
    	)


def search_travel_form(request):
    context = RequestContext(request)
    print("HEY")
    if request.method == 'GET':
        print("lalal")
        print(request.GET)
        # create a form instance and populate it with data from the request:
        form_1 = SearchTravelForm(request.POST)
        form = SearchTravelForm()
        country = request.GET['country_choose']
        print("Close")
        activity = request.GET['activity_choose']
        min_cost = request.GET['min_cost']
        max_cost = request.GET['max_cost']
        # check whether it's valid:a
        #return redirect('/catalog/search/category=travel&country=' + str(country)+'&activity=' +str(activity)+'&min_cost='+str(min_cost)+'&max_cost=' + max_cost)
    else:
        form = SearchTravelForm()
    if country == '':
        countries = list(Country.objects.all())
    else:
        try:
            countries = [Country.objects.get(id=country)]
        except ObjectDoesNotExist:
            countries = {}
    if activity == '':
        activities = list(Active.objects.all())
    else:
        try:
            activities = [Active.objects.get(id=activity)]
        except ObjectDoesNotExist:
            activities = {}
    if min_cost == '':
        min_cost == 0
    if max_cost == '':
        cost = Trip.objects.all()
    else:
        cost = Trip.objects.filter(thing__cost__gte=int(min_cost), thing__cost__lte=int(max_cost))
    try:
        travels = cost.filter(country__in=countries, active__in=activities)
    except ObjectDoesNotExist:
        travels = {}
    context = RequestContext(request)
    form = SearchTravelForm()
    print("Render")
    return render_to_response(
        "shop/catalog_new.html",
        {'search_travel_form': form, 'items':travels},
        context_instance=context)

def redirect_country(request, country):
    request.GET = {'country_choose':'1'}
    a = search_travel_form(request)
    return a

def order_1(request):
    return render(request, 'shop/order_registration_1.html')

def order_2(request):
    return render(request, 'shop/order_registration_2.html')

def order_3(request):
    return render(request, 'shop/order_registration_3.html')

def order_4(request):
    return render(request, 'shop/order_registration_4.html')


def find_by_text(request):
    text = request.GET.get('search')
    print("Hey", text)
    items_1 = set(Trip.objects.filter(active__name__icontains=text))
    items_2 = set(Trip.objects.filter(country__name__icontains=text))
    items_3 = set(Trip.objects.filter(thing__information__icontains=text))
    items_4 = set(Trip.objects.filter( thing__additional_information__icontains=text))
    items = set(chain(items_1, items_2, items_3, items_4))
    search_form = FormSearch()
    return render(request, 'shop/find_by_text.html',{'items': items, 'text':text, 'search_form':search_form, 'count':len(items)})

@csrf_exempt
def your_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)

        if form.is_valid():


            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:

            return HttpResponseRedirect('/your_name/' + form.cleaned_data['min_cost'])
            return HttpResponseRedirect('/your_name/' + request.POST['min_cost'])

    # if a GET (or any other method) we'll create a blank form
    else:
        pass
    return render(request, 'name.html', {'form': form})


def search_item(request,type, min_cost, max_cost):
    print("hey", type)
    if type == '':
        types = list(Good_type.objects.all())
    else:
        try:
            types = [Good_type.objects.get(name=type)]
        except ObjectDoesNotExist:
            types = []
    if min_cost == '':
        min_cost == 0
    if max_cost == '':
        cost = Item.objects.all()
    else:
        cost = Item.objects.filter(thing__cost__gte=int(min_cost), thing__cost__lte=int(max_cost))

    try:
        items = cost.filter(type__in=types)
    except ObjectDoesNotExist:
        items = {}
    context = RequestContext(request)
    form = SearchItemlForm()
    print(items)
    return render(request, 'shop/catalog_new.html',{'items': items, 'search_item_form':form,'my_path':request.path})


def search_travel(request,country, activity, min_cost, max_cost):
    if country == '':
        countries = list(Country.objects.all())
    else:
        try:
            countries = [Country.objects.get(name=country)]
        except ObjectDoesNotExist:
            countries = {}
    if activity == '':
        activities = list(Active.objects.all())
    else:
        try:
            activities = [Active.objects.get(name=activity)]
        except ObjectDoesNotExist:
            activities = {}
    if min_cost == '':
        min_cost == 0
    if max_cost == '':
        cost = Trip.objects.all()
    else:
        cost = Trip.objects.filter(thing__cost__gte=int(min_cost), thing__cost__lte=int(max_cost))
    try:
        travels = cost.filter(country__in=countries, active__in=activities)
    except ObjectDoesNotExist:
        travels = {}
    context = RequestContext(request)
    form = SearchTravelForm()
    return render(request, 'shop/catalog_new.html',{'items': travels, 'search_travel_form':form,'my_path':request.path})

# def search(request):
#     if request.method == 'GET':
#         form = SearchForm(data=request.GET)
#         if form.is_valid():
#             ele = 0
#         else:
#             form = SearchForm()

def item(request, my_id):
	return render(request, 'shop/item.html', {'countr': Country.objects.all(), 'act': Active.objects.all(), 'types':Good_type.objects.all(), 'item': Thing.objects.get(id=my_id)})

def log(request):

    if request.user.is_authenticated():
        return HttpResponse("<button class=\"btn btn-default btn-lg\" style=\"font-family:'Poiret One;'\" data-toggle=\"modal\" data-target=\".bs-logout-modal-sm\" ><span class =\"fa fa-fw fa-user\"></span > <my_button>Выйти</my_button></button> </button >")
    else:
        return HttpResponse("<a href='/login/'><button type=\"button\" class=\"btn btn-default btn-lg\" style=\"font-family:'Poiret One;'\" ><span class =\"fa fa-fw fa-user\"></span >  <my_button>Вход</my_button></button></a>")
        #return HttpResponse("<a href='/login/vk-oauth2/'>login with VK</a>")


@login_required
def account_profile(request):
    return HttpResponse("Hi, {0}! Nice to meet you.".format(request.user.first_name))


def account_logout(request):

    logout(request)
    return redirect('/')

#регистрация
def register(request):
    # Like before, get the request's context.
    context = RequestContext(request)

    # A boolean value for telling the template whether the registration was successful.
    # Set to False initially. Code changes value to True when registration succeeds.
    registered = False

    # If it's a HTTP POST, we're interested in processing form data.
    if request.method == 'POST':
        # Attempt to grab information from the raw form information.
        # Note that we make use of both UserForm and UserProfileForm.
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)

        # If the two forms are valid...
        if user_form.is_valid() and profile_form.is_valid():
            # Save the user's form data to the database.
            user = user_form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.save()

            # Now sort out the UserProfile instance.
            # Since we need to set the user attribute ourselves, we set commit=False.
            # This delays saving the model until we're ready to avoid integrity problems.
            profile = profile_form.save(commit=False)
            profile.user = user

            # Did the user provide a profile picture?
            # If so, we need to get it from the input form and put it in the UserProfile model.
            # if 'photo' in request.FILES:
            #     request.FILES['photo'].name = request.POST['username'] + os.path.splitext(request.FILES['photo'].name)[1]
            #     profile.photo = request.FILES['photo']
            # else:
            #     resource = generator.generate(
            #         request.POST['username'],
            #         320,
            #         320,
            #         output_format="png",
            #         padding=(10,10,10,10)
            #     )
            #     image = open(
            #         os.path.join(djangoSettings.STATICFILES_DIRS[2], request.POST['username'] + str('.png')),
            #         "wb")
            #     image.write(resource)
            #     image.close()
            #     profile.picture = os.path.join(djangoSettings.STATIC_URL, 'avatars', request.POST['username'] + str('.png'))


            # Now we save the UserProfile model instance.
            profile.save()

            # Update our variable to tell the template registration was successful.
            registered = True

        # Invalid form or forms - mistakes or something else?
        # Print problems to the terminal.
        # They'll also be shown to the user.
        else:
            print(user_form.errors, profile_form.errors)

    # Not a HTTP POST, so we render our form using two ModelForm instances.
    # These forms will be blank, ready for user input.
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    # Render the template depending on the context.
    return render_to_response(
            'shop/registration.html',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered},
            context)
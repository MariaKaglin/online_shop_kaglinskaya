# -*- coding: utf-8 -*- 
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django import forms
from taggit.managers import TaggableManager
from django.forms import ModelChoiceField


#class User(models.Model): need it..?
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    #additional information
    name = models.CharField(max_length=30, default="username")
    photo = models.ImageField(upload_to='users')
    information = models.CharField(max_length=200)
    country = models.CharField(max_length=30)
    town = models.CharField(max_length=20)
    def __str__(self):
        return self.name
      

class Country(models.Model):
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name
        #return '%s' % self.name.decode('utf-8')

class Active(models.Model):
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name
        #return '%s' % self.name.decode('utf-8')

# class Good_type(models.Model):
#     name = models.CharField(max_length=30)

class Thing(models.Model):
    name = models.CharField(max_length=30)
    cost = models.IntegerField(default=0)
    photo = models.ImageField(upload_to='items', default="shop/static/img/a.png")
    information = models.CharField(max_length=150)
    additional_information = models.CharField(max_length=400, default="")
    seller = models.ForeignKey(UserProfile)
    raiting = models.IntegerField(default=0)
    tags = TaggableManager()
    def __str__(self):
        return self.name

    
class Trip(models.Model):
    thing = models.OneToOneField(Thing)
    country = models.ForeignKey(Country)
    active = models.ForeignKey(Active)
    def __str__(self):
        return self.thing.name

# class Item(models.Model):
#     thing = models.OneToOneField(Thing)
#     type = models.ForeignKey(Good_type)
#     def __str__(self):
#         return self.name

class Sale(models.Model):
    thing = models.ManyToManyField(Thing)
    def __str__(self):
        return self.name

class Recall(models.Model):
    name = models.CharField(max_length=30)
    text = models.CharField(max_length=200)
    to = models.ForeignKey(Thing)
    def __str__(self):
        return self.name


class Search(models.Model):
    country_choose = models.ManyToManyField(Country)
    activity_choose = models.ManyToManyField(Active)
    min_cost = models.IntegerField(default=0)
    max_cost = models.IntegerField(default=0)


class FormSearch(forms.Form):
    search = forms.CharField( max_length=20)


#регистрация
# class SearchForm(forms.Form):
#     name = models.CharField(max_length=30, default = "")
#     def clean_name(self):
#         name = self.cleaned_data['name']
#         if isinstance(name, str):
#             name = name.encode('ascii', 'xmlcharrefreplace')
#             if not 3 <= len(name) <= 30:
#                 raise ValidationError(
#                 ('%(value)s Имя дложно быть длиннее 2 и короче 200 символов!') % {'value': 'Name'},
#                 code='invalid',
#                 )
#             return name
#         else:
#             raise ValidationError(
#                 ('Invalid value: %(value)s') % {'value': 'Name'},
#                 code='invalid',
#             )
#     status = forms.ChoiceField(widget=forms.Select(),
#                                 required=True, choices =  (('1', 'First',), ('2', 'Second',)))
#     #choice_country = forms.ChoiceField(widget=forms.RadioSelect, choices= ('BT-J1KND-A', 'BT-J1KND-A'))
#     #choice_country = forms.ChoiceField(widget=forms.RadioSelect, choices=Country.objects.all())
#     choice_activity = forms.ChoiceField(widget=forms.RadioSelect, choices=Active.objects.all())


class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)
    min_cost = forms.NumberInput()
    country_choose = forms.Select()


class Search_Form(forms.Form):
    country_choose = forms.Select(choices=Country.objects.all())
    activity_choose = forms.Select(choices=Active.objects.all())
    min_cost = forms.IntegerField()
    max_cost = forms.IntegerField()


class SearchTravelForm(forms.ModelForm):
    class Meta:
        model = Search
        fields = ('country_choose', 'activity_choose', 'min_cost', 'max_cost')
        widgets = {
            'activity_choose': forms.Select(choices=Active.objects.all()),
            'country_choose': forms.Select(choices=Country.objects.all()),
            'min_cost': forms.NumberInput(  ),
            'max_cost': forms.NumberInput(),
        }
        labels = {
            'activity_choose': "Вид отдыха",
            'country_choose': "Страна",
            'max_cost': "Максимальная стоимость",
            'min_cost': "Минимальная стоимость",
        }
    # country_choose = forms.Select()
    # activity_choose = forms.Select(choices=('hi', 'hi12'))
    # min_cost = forms.NumberInput(attrs={'step': '1'})
    # your_name = forms.CharField(label='Your name', max_length=100)

# class SearchItemlForm(forms.ModelForm):
#     class Meta:
#         model = Search
#         fields = ('country_choose', 'min_cost', 'max_cost')
#         widgets = {
#             'country_choose': forms.Select(choices=Good_type.objects.all()),
#             'min_cost': forms.NumberInput(),
#             'max_cost': forms.NumberInput(),
#         }
#         labels = {
#             'country_choose': "Категория товара",
#             'max_cost': "Максимальная стоимость",
#             'min_cost': "Минимальная стоимость",
#         }
class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('name', 'country', 'town','information')#'photo')

    def clean_name(self):
        name = self.cleaned_data['name']
        if isinstance(name, str):
            name = name.encode('ascii', 'xmlcharrefreplace')
            if not 3 <= len(name) <= 30:
                raise ValidationError(
                ('%(value)s Имя дложно быть длиннее 2 и короче 200 символов!') % {'value': 'Name'},
                code='invalid',
                )
            return name
        else:
            raise ValidationError(
                ('Invalid value: %(value)s') % {'value': 'Name'},
                code='invalid',
            )

    def clean_country(self):
        country = self.cleaned_data['country']
        if isinstance(country, str):
            country = country.encode('ascii', 'xmlcharrefreplace')
            if not 3 <= len(country) <= 30:
                raise ValidationError(
                ('%(value)s Название страны дложно быть длиннее 2 и короче 30 символов!') % {'value': 'Country'},
                code='invalid',
                )
            return country
        else:
            raise ValidationError(
                ('Invalid value: %(value)s') % {'value': 'Country'},
                code='invalid',
            )
    
    def clean_town(self):
        town = self.cleaned_data['town']
        if isinstance(town, str):
            town = town.encode('ascii', 'xmlcharrefreplace')
            if not 3 <= len(town) <= 30:
                raise ValidationError(
                ('%(value)s Название города дложно быть длиннее 2 и короче 20 символов!') % {'value': 'Town'},
                code='invalid',
                )
            return town
        else:
            raise ValidationError(
                ('Invalid value: %(value)s') % {'value': 'Town'},
                code='invalid',
            )

    def clean_information(self):
        information = self.cleaned_data['information']
        if isinstance(information, str):
            information = information.encode('ascii', 'xmlcharrefreplace')
            if not 3 <= len(information) <= 30:
                raise ValidationError(
                ('%(value)s Информация должна быть длиннее 2 и короче 200 символов!') % {'value': 'Information'},
                code='invalid',
                )
            return information
        else:
            raise ValidationError(
                ('Invalid value: %(value)s') % {'value': 'Information'},
                code='invalid',
            )    

    # def clean_photo(self):
    #     photo= self.cleaned_data['photo']
    #     if avatar:
    #         w, h = get_image_dimensions(photo)
    #     else:
    #         return

    #     #validate dimensions
    #     max_width = max_height = 500
    #     if w > max_width or h > max_height:
    #         raise forms.ValidationError(
    #             u'Пожалуйста, выберите фото, которое'
    #              '%s x %s px или меньше.' % (max_width, max_height))

    #     #validate content type
    #     main_type, sub = avatar.content_type.split('/')
    #     if not (main_type == 'image' and sub in ['jpeg', 'pjpeg', 'gif', 'png']):
    #         raise forms.ValidationError(u'Пожалуйста, загрузите  JPEG, '
    #             'GIF или PNG изображение.')

    #     #validate file size
    #     if len(avatar) > (1024 * 1024):
    #         raise forms.ValidationError(
    #             u'Слишком большой размер фото')
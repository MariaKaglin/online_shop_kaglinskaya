from django.conf.urls import url, include
from . import views
from django.contrib.auth.views import login
import social.apps.django_app.urls


urlpatterns = [
    url(r'^$', views.main),
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^accounts/logout/$', 'shop.views.account_logout', name='logout'),
    url(r'^accounts/login/$', 'shop.views.log', name='log'),
    url(r'^accounts/profile/$', 'shop.views.account_profile', name='profile'),
    #url(r'', include(pat, namespace='social')),
    url(r'^your_name/', 'shop.views.your_name'),
    url(r'^basket/$', views.basket),
    url(r'^add-to-basket/$', views.add, name='add'),
    url(r'^search_travel_form/$', views.search_travel_form, name='search_travel_form'),
    url(r'^catalog/search/category=travel&country=(?P<country>[a-zA-Z0-9]*)&activity=(?P<activity>[a-zA-Z0-9]*)&min_cost=(?P<min_cost>[0-9]*)&max_cost=(?P<max_cost>[0-9]*)', views.search_travel, name='search_travel'),
    url(r'^get_data$',views.get_data, name='get_data'),
    url(r'register/',  'shop.views.register', name='registration'),
    url(r'^item/([0-9]+)/$', views.item),
    url(r'find_by_text/', 'shop.views.find_by_text', name = "find"),
    url(r'^redirect_country_(?P<country>[0-9]*)/', 'shop.views.redirect_country'),
    url(r'', include(social.apps.django_app.urls, namespace='social')),
    url(r'order_step_1', views.order_1),
    url(r'order_step_2', views.order_2),
    url(r'order_step_3', views.order_3),
    url(r'order_step_4', views.order_4),

]